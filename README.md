Advisory of Avast Ticket-ID QAD-408-56721:
- Avast AntiVirus (aswSnx.sys) IOCTL 0x82AC0060 Out-of-Bound Write
- No CVE is assigned
- [UPDATE] CVE-2020-20118 is reserved  (see timeline)

Timeline:
- 2019-07-24 Reported vulnerability and POC to Avast
- 2019-07-26 Avast acknowledged report and assigned ticket-ID QAD-408-56721
- 2019-08-16 Avast replied that this vulnerability is fixed in v19.7
- 2019-08-21 Requested CVE ID from Mitre
- 2023-06-21 Mitre notified that CVE-2020-20118 is reserved 

Reference:
- https://cve.mitre.org/cgi-bin/cvename.cgi?name=2020-20118
